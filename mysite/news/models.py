from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class News(models.Model):
    """Новости"""
    title = models.CharField(max_length=150, verbose_name='Наименование')  # verbose_name название полей в админке
    content = models.TextField(blank=True, verbose_name='Контент')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/', verbose_name='Фото', blank=True)
    is_published = models.BooleanField(default=True, verbose_name='Опубликовано')
    category = models.ForeignKey('Category', on_delete=models.PROTECT,
                                 verbose_name='Категория')
    views = models.IntegerField(default=0, verbose_name='Просмотры')
    author = models.ForeignKey(User, blank=True, null=True, on_delete=models.PROTECT,
                               verbose_name='Автор')

    # PROTECT защищает связанные модели от удаления, null позволяет не заполнять бд этим параметром, по умолчанию False
    # default=1 позволяет по умолчанию ставить всем news 1 категорию, если мы не выбираем категорию
    # С помощью ForeignKey связывает модель News и модель 'Category'
    # verbose_name переименовывает категории в админ панели News

    def get_absolute_url(self):  # по соглашению так называть, добавляет в админке кнопку "смотреть на сайте"
        return reverse('view_news', kwargs={"pk": self.pk})  # category берётся из urls

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Новость'  # название ДБ в админке
        verbose_name_plural = 'Новости'
        ordering = ['-created_at']  # порядок новостей в админке


class Category(models.Model):
    """Категории новостей"""
    title = models.CharField(max_length=150, db_index=True, verbose_name='Наименование категории')

    # db_index индексирует это поле, более быстро для поиска

    def get_absolute_url(self):  # по соглашению так называть, добавляет в админке кнопку "смотреть на сайте"
        return reverse('category', kwargs={"category_id": self.pk})  # category берётся из urls

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ['title']


class RatingStar(models.Model):
    """Звёзды рейтинга"""
    value = models.SmallIntegerField("Значение", default=0)

    def __str__(self):
        return f'{self.value}'

    class Meta:
        verbose_name = "Звезда рейтинга"
        verbose_name_plural = "Звезды рейтинга"
        ordering = ["-value"]


class Rating(models.Model):
    """Рейтинг"""
    ip = models.CharField("IP адрес", max_length=15)
    star = models.ForeignKey(RatingStar, on_delete=models.CASCADE, verbose_name="Звезда")
    news = models.ForeignKey(News, on_delete=models.CASCADE, verbose_name="Новость", related_name='news_r')

    def __str__(self):
        return f"{self.star} - {self.news}"

    class Meta:
        verbose_name = "Рейтинг"
        verbose_name_plural = "Рейтинги"


class Reviews(models.Model):
    """Комментарии"""
    name = models.ForeignKey(User, blank=True, null=True, on_delete=models.PROTECT,
                             verbose_name='Автор комментария')
    text = models.TextField('Сообщение')
    parent = models.ForeignKey('self', verbose_name='Родитель', on_delete=models.SET_NULL, blank=True, null=True)
    news = models.ForeignKey(News, verbose_name='Новость', on_delete=models.CASCADE, related_name='reviews_n')
    created_reviews = models.DateTimeField(auto_now_add=True, verbose_name='Время написания комментария')

    def __str__(self):
        return f'{self.name} - {self.news}'

    def get_absolute_url(self):
        return reverse('post_detail', kwargs={"id": self.pk})

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        ordering = ["-created_reviews"]
