from django.contrib import messages
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.views import View

from news.forms import ReviewForm, RatingForm, ContactForm
from news.models import News, Rating, Reviews


def contact(request):
    """Связь пользователя с разработчиком"""
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            mail = send_mail(form.cleaned_data['subject'], form.cleaned_data['content'], 'kylll@mail.ru',
                             ['ololoe228@gmail.com@gmail.com'], fail_silently=False)
            if mail:
                messages.success(request, 'Письмо отправлено!')
                return redirect('contact')
            else:
                messages.error(request, 'Ошибка отправки')
        else:
            messages.error(request, 'Ошибка валидации')
    else:
        form = ContactForm()
    return render(request, 'news/test.html', {"form": form})


class AddStarRating(View):
    """Добавление звёзд рейтинга"""

    def get_client_ip(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    def post(self, request):
        form = RatingForm(request.POST)
        if form.is_valid():
            Rating.objects.update_or_create(
                ip=self.get_client_ip(request),
                news_id=int(request.POST.get("news")),
                defaults={'star_id': int(request.POST.get("star"))}
            )
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)


class AddReview(View):
    """Отзывы"""

    def post(self, request, pk):
        form = ReviewForm(request.POST)
        news = News.objects.get(id=pk)
        if form.is_valid():
            form = form.save(commit=False)
            form.news = news
            form.name = request.user
            form.save()
            return redirect(news.get_absolute_url())
        else:
            return HttpResponse(status=400)


def delete_reviews(request, reviews_id=None):
    """Удаление комментариев"""
    reviews_to_delete = Reviews.objects.get(id=reviews_id)
    reviews_to_delete.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
