


# def register(request):
#     if request.method == 'POST':
#         form = UserRegisterForm(request.POST)
#         if form.is_valid():
#             user = form.save()
#             login(request, user)
#             messages.success(request, 'Вы успешно зарегистрировались!')
#             return redirect('home')
#         else:
#             messages.error(request, 'Ошибка регистрации')
#     else:
#         form = UserRegisterForm()
#     return render(request, 'news/register.html', {"form": form})





# def user_login(request):
#     if request.method == 'POST':
#         form = UserLoginForm(data=request.POST)  # data в логине обязательна
#         if form.is_valid():
#             user = form.get_user()  # специальный метод гет_юзер
#             login(request, user)  # специальный метод логин импортируемый
#             return redirect('home')
#     else:
#         form = UserLoginForm
#     return render(request, 'news/login.html', {'form': form})








# def get_category(request, category_id):
#    news = News.objects.filter(category_id=category_id)  # filter = WHERE в SQL
#    category = Category.objects.get(pk=category_id)  # В переменную записываем айди категории
#    return render(request, 'news/category.html', {'news': news, 'category': category})




#   context_object_name = 'news_item' в DetailView в шаблоне можно переименовать всё в object вместо news_item


# class ViewRating(ListView):
#     model = Rating
#     template_name = 'news/home.html'
#     context_object_name = 'rating'
#
#     def get_context_data(self, *, object_list=None, **kwargs):
#         context = super().get_context_data(**kwargs)  # сохраняем данные в родительский класс, что бы не затёрлись
#         context['star'] = Rating.objects.get(pk=self.kwargs['news_id'])
#         context['ip'] = 1
#         return context








# def view_news(request, news_id):
#    news_item = News.objects.get(pk=news_id)
#    news_item = get_object_or_404(News, pk=news_id)
#    return render(request, 'news/view_news.html', {"news_item": news_item})


#   success_url = reverse_lazy('home') Используется для редиректа после добавления новости,
# если в модели News не определён метод get_absolute_url

# def add_news(request):
#    if request.method == 'POST':
#        form = NewsForm(request.POST)
#        if form.is_valid():
#            news = form.save()
#            return redirect(news)
#    else:
#        form = NewsForm()
#    return render(request, 'news/add_news.html', {'form': form})


# def post_detail(request, slug):
#     template_name = 'view_news.html'
#     news = get_object_or_404(News, slug=slug)
#     comments =
#     new_comment = None
#     if request.method == 'POST':
#         comment_form = CommentForm(data=request.POST)
#         if comment_form.is_valid():
#             new_comment = comment_form.save(commit=False)
#             new_comment.news = news
#             new_comment.save()
#     else:
#         comment_form = CommentForm()
#
#     return render(request, template_name, {'news': news,
#                                            'comments': comments,
#                                            'new_comment': new_comment,
#                                            'comment_form': comment_form})

