from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count, Avg
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic import CreateView, DetailView, ListView
from news.forms import NewsForm, RatingForm
from news.models import News, Category


class NewsByCategory(ListView):
    """Новости, отфильтрованные по категориям"""
    model = News
    template_name = 'news/home.html'
    context_object_name = 'news'
    allow_empty = False  # выдаёт 404 ошибку при не существующей или пустой категории
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)  # сохраняем данные в родительский класс, что бы не затёрлись
        context['title'] = Category.objects.get(pk=self.kwargs['category_id'])
        return context

    def get_queryset(self):  # фильтруем записи, которые "опубликованы"
        return News.objects.annotate(comments_count=Count('reviews_n')).filter(is_published=True,
                                                                               category_id=self.kwargs[
                                                                                   'category_id']).select_related(
            'category').order_by('-created_at')


class HomeNews(ListView):
    """Домашняя страница"""
    model = News  # пишем связанную модель
    template_name = 'news/home.html'  # путь к шаблону html
    context_object_name = 'news'  # переменная в шаблоне
    paginate_by = 3  # количество постов на странице

    # extra_context = {'title': 'Главная страница'}  # статические файлы в шаблоне

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)  # сохраняем данные в родительский класс, что бы не затёрлись
        context['title'] = 'Главная страница'
        context['comment'] = News.objects.annotate(comments_count=Count('reviews_n')).all().order_by('-created_at')
        return context

    def get_queryset(self):  # фильтруем записи, которые "опубликованы"
        return News.objects.filter(is_published=True).select_related('category').annotate(
            rating=Avg('news_r__star__value')).order_by('-created_at')


class ViewNews(DetailView):
    """Страница конкретной новости"""
    model = News
    template_name = 'news/view_news.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['star_form'] = RatingForm()
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.views += 1
        self.object.save()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class CreateNews(LoginRequiredMixin, CreateView):
    """Добавление новости"""
    form_class = NewsForm
    template_name = 'news/add_news.html'

    def post(self, request, **kwargs):
        form = NewsForm(request.POST, request.FILES)
        if form.is_valid():
            new_post = form.save(commit=False)
            new_post.author = request.user
            new_post.save()
            return redirect('home')
        else:
            return HttpResponse(status=400)


def delete_news(request, news_id=None):
    """Удаление новости"""
    news_to_delete = News.objects.get(id=news_id)
    news_to_delete.delete()
    return redirect('home')
