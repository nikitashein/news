from django.contrib.auth import login, logout
from django.contrib.auth.views import LoginView
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView

from news.forms import UserLoginForm, UserRegisterForm
from news.utils import DataMixin


class LoginUser(DataMixin, LoginView):
    """Авторизация на сайте"""
    form_class = UserLoginForm
    template_name = 'news/login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        get_us_cont = self.get_user_context()
        return dict(list(context.items()) + list(get_us_cont.items()))

    def get_success_url(self) -> str:
        return reverse_lazy('home')


class RegisterUser(DataMixin, CreateView):
    """Регистрация пользователей"""
    form_class = UserRegisterForm
    template_name = 'news/register.html'
    success_url = reverse_lazy('login')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title='Регистрация')
        return dict(list(context.items()) + list(c_def.items()))

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('home')


def user_logout(request):
    logout(request)  # отвечает за выход из аккаунта
    return redirect('login')
