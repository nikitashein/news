from django.urls import path
from .views import *

urlpatterns = [
    path('', HomeNews.as_view(), name='home'),
    path('category/<int:category_id>/', NewsByCategory.as_view(),
         name='category'),
    # int то что передаётся число, <> для динамики
    path('news/<int:pk>/', ViewNews.as_view(), name='view_news'),
    path('news/add-news/', CreateNews.as_view(), name='add_news'),
    path('register/', RegisterUser.as_view(), name='register'),
    path('login/', LoginUser.as_view(), name='login'),
    path('logout/', user_logout, name='logout'),
    path('contact/', contact, name='contact'),
    path("add-rating/", AddStarRating.as_view(), name='add_rating'),
    path('review/<int:pk>/', AddReview.as_view(), name='add_rewiew'),

    path('delete/<news_id>', delete_news, name='delete_news'),
    path('delete/comment/<reviews_id>', delete_reviews, name='delete_reviews'),
]
