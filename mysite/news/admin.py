from .models import *
from django import forms
from django.contrib import admin
from ckeditor_uploader.widgets import CKEditorUploadingWidget


# class NewsAdminForm(forms.ModelForm):
#     """Форма новости"""
#     content = forms.CharField(widget=CKEditorUploadingWidget())
#
#     class Meta:
#         model = News
#         fields = '__all__'


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    """Новости"""
    # form = NewsAdminForm
    list_display = (
        'id', 'title', 'category', 'author', 'created_at', 'updated_at',
        'is_published')  # Какие поля показываются в админке
    list_display_links = ('id', 'title')  # на какие поля можно нажать для редактирования "новости"
    search_fields = ('title', 'content', 'author')  # добавляет поле для поиска в админке по названию и контенту
    list_editable = ('is_published',)  # менять не заходя в новость
    readonly_fields = ('author', 'views')
    list_filter = ('is_published', 'category')  # Добавляет возможность фильтровать по категориям и публикации в админке
    # https://www.youtube.com/watch?v=XYl2su5KO50&list=PLmC7X4gkQWCeyIdLxHZdts-3tkcrxP4-o&index=41&ab_channel=Progercourse
    # ссылка на фото в админке


@admin.register(Category)  # этот декоратор заменяет нижнюю строчку регистрации
class CategoryAdmin(admin.ModelAdmin):
    """Категории"""
    list_display = ('id', 'title')
    list_display_links = ('id', 'title')
    search_fields = ('title',)  # Обязательно ставим запятую, тк это кортеж


@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin):
    """Рейтинг"""
    list_display = ("star", "news", "ip")


@admin.register(Reviews)
class ReviewsAdmin(admin.ModelAdmin):
    """Комментарии"""
    list_display = ("news", "text", "name")
    readonly_fields = ('name',)


# admin.site.register(News, NewsAdmin)  # регистрируем модель в админке и добавляем модель с доп параметрами админа
# admin.site.register(Category, CategoryAdmin)

admin.site.register(RatingStar)
admin.site.site_title = 'Управление новостями'  # изменяем тайтл в админке
admin.site.site_header = 'Управление новостями'  # изменяем заголовок в админке
