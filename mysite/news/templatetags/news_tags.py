from django import template
from django.db.models import Count, F, Avg, Sum
from news.models import Category, News

register = template.Library()


@register.simple_tag(name='get_list_categories')
def get_categories():
    return Category.objects.all()


@register.inclusion_tag('news/list_categories.html')
def show_categories():
    categories = Category.objects.annotate(cnt=Count('news', filter=F('news__is_published'))).filter(cnt__gt=0)
    # прописали что бы в категорях не было пустых новостей
    # класс F делает фильтр для отображения только опубликованных новостей
    return {"categories": categories}
