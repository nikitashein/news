from .models import Category


class DataMixin:
    """Миксин для auth_views.py"""

    def get_user_context(self, **kwargs):
        context = kwargs
        category = Category.objects.all()
        context['category'] = category
        return context
