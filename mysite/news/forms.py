import re
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.core.exceptions import ValidationError
from .models import *
from captcha.fields import CaptchaField


class RatingForm(forms.ModelForm):
    """Рейтинговая форма"""
    star = forms.ModelChoiceField(queryset=RatingStar.objects.all(), widget=forms.RadioSelect(), empty_label=None)

    class Meta:
        model = Rating
        fields = ('star',)


class ContactForm(forms.Form):
    """Связь с разработчиками"""
    subject = forms.CharField(label='Тема', widget=forms.TextInput(attrs={'class': 'form-control'}))
    content = forms.CharField(label='Текст', widget=forms.Textarea(attrs={'class': 'form-control', "rows": 5}))
    captcha = CaptchaField()


class UserLoginForm(AuthenticationForm):
    """Форма авторизации"""
    username = forms.CharField(label='Имя пользователя', widget=forms.TextInput(attrs={"class": "form-control"}))
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={"class": "form-control"}))


# создаём эту форму для регистрации в views
class UserRegisterForm(UserCreationForm):
    """Форма регистрации"""
    username = forms.CharField(label='Имя пользователя', help_text='Подсказка',
                               widget=forms.TextInput(attrs={"class": "form-control"}))
    email = forms.EmailField(label='E-mail', widget=forms.EmailInput(attrs={"class": "form-control"}))
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={"class": "form-control"}))
    password2 = forms.CharField(label='Подтверждение пароля',
                                widget=forms.PasswordInput(attrs={"class": "form-control"}))

    # help_text выводит текс возле поля
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')


class NewsForm(forms.ModelForm):
    """Форма создания новости"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].empty_label = 'Выберите категорию'  # добавляет в селект вместо ----

    class Meta:
        model = News  # модель с которой связана
        fields = ['title', 'content', 'is_published', 'photo', 'category']  # поля которые нужны
        widgets = {  # передаём атрибуты и типы полей
            'title': forms.TextInput(attrs={"class": "form-control"}),
            'content': forms.Textarea(attrs={"class": "form-control", "rows": 5}),
            'category': forms.Select(attrs={"class": "form-control"}),
        }

    def clean_title(self):
        title = self.cleaned_data['title']  # получаем данные из словара cleaned_data
        if re.match(r'\d', title):  # если title начинается с чисел
            raise ValidationError('Название не должно начинаться с цифры')
        return title


class ReviewForm(forms.ModelForm):
    """Форма отзывов"""

    class Meta:
        model = Reviews
        fields = ("text", )

